package com.func;

import com.func.creational.builder.Client;
import com.func.model.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("<<< GOF >>>");

        /*
//        PizzaStore ps = new CbbaPizzaStore();
        PizzaStore ps = new SczPizzaStore();

        Pizza p = ps.order(PizzaType.VEGGIE);

        if (p == null) {
            System.out.println("The order failed!");
        } else {
            System.out.println("You've ordered a " + p.getName());
        }*/

//        Client.test();
//        com.func.structural.composite.Client.test();

//        com.func.behavioral.memento.Client.test();
        com.func.structural.decorator.Client.test();
    }
}
