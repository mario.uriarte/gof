package com.func.behavioral.memento;

import com.func.model.Car;

public class Client {

    public static void test() {

        System.out.println("Hi from memento app");

        Car car1 = new Car(
                "2018",
                "19000$",
                "Red",
                "Mazda"
        );
        car1.showCar();

        Protector caretaker = new Protector();
        caretaker.addMemento(car1.saveMemento());

        car1.setColor("Blue");
        car1.setPrice("15000$");
        car1.showCar();

        Car carOldInfo = car1.restoreMemento(caretaker.getMemento());
        carOldInfo.showCar();

    }
}
