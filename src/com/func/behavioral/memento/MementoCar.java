package com.func.behavioral.memento;

import com.func.model.Car;

public class MementoCar {

    private String model;
    private String price;
    private String color;
    private String brand;

    public MementoCar(String model, String price, String color, String brand) {
        this.model = model;
        this.price = price;
        this.color = color;
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Car getState() {
        Car car = new Car(
                this.model,
                this.price,
                this.color,
                this.brand
        );
        return car;
    }
}
