package com.func.behavioral.memento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Protector {

    private List<MementoCar> history = new ArrayList<MementoCar>();
//    private Map<Integer, MementoCar> history2;

    public void addMemento(MementoCar m) {
        history.add(m);
    }

    public MementoCar getMemento() {
        MementoCar m = history.get(history.size() - 1);
        return m;
    }
}
