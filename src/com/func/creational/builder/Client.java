package com.func.creational.builder;

import com.func.creational.builder.builder.CabinHouseBuilder;
import com.func.creational.builder.builder.DetachedHouseBuilder;
import com.func.creational.builder.director.CivilEngineer;
import com.func.creational.builder.part.basement.ConcreteBasement;
import com.func.creational.builder.part.interior.ClassicStyleInterior;
import com.func.creational.builder.part.interior.ModernStyleInterior;
import com.func.creational.builder.part.roof.HayRoof;
import com.func.creational.builder.part.roof.MetalRoof;
import com.func.creational.builder.part.structure.MetalStructure;
import com.func.creational.builder.part.structure.WoodenStructure;
import com.func.creational.builder.product.House;

public class Client {
    public static void test() {

        System.out.println("Hola desde builder");

        CivilEngineer ce = new CivilEngineer(new DetachedHouseBuilder());
        displayHouse(ce.construct());

        System.out.println("\n********************************\n");
        ce.setHouseBuilder(new CabinHouseBuilder());
        displayHouse(ce.construct());
    }

    public static void displayHouse(House house) {
        System.out.println("House specs: ");
        System.out.println("Basement: " + (house.getBasement() == null ? "-" : house.getBasement().getDescription()));
        System.out.println("Structure: " + (house.getStructure() == null ? "-" : house.getStructure().getDescription()));
        System.out.println("Roof: " + (house.getRoof() == null ? "-" : house.getRoof().getDescription()));
        System.out.println("Interior: "+ (house.getInterior() == null ? "-" : house.getInterior().getDescription()));
    }
}
//43.46