package com.func.creational.builder.builder;

import com.func.creational.builder.part.basement.Basement;
import com.func.creational.builder.part.interior.ClassicStyleInterior;
import com.func.creational.builder.part.interior.Interior;
import com.func.creational.builder.part.roof.HayRoof;
import com.func.creational.builder.part.roof.Roof;
import com.func.creational.builder.part.structure.Structure;
import com.func.creational.builder.part.structure.WoodenStructure;

public class CabinHouseBuilder extends HouseBuilder {

    @Override
    public Basement getBasement() {
        return null;
    }

    @Override
    public Structure getStructure() {
        return new WoodenStructure();
    }

    @Override
    public Roof getRoof() {
        return new HayRoof();
    }

    @Override
    public Interior getInterior() {
        return new ClassicStyleInterior();
    }
}
