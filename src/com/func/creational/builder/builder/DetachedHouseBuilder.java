package com.func.creational.builder.builder;

import com.func.creational.builder.part.basement.Basement;
import com.func.creational.builder.part.basement.ConcreteBasement;
import com.func.creational.builder.part.interior.Interior;
import com.func.creational.builder.part.interior.ModernStyleInterior;
import com.func.creational.builder.part.roof.MetalRoof;
import com.func.creational.builder.part.roof.Roof;
import com.func.creational.builder.part.structure.MetalStructure;
import com.func.creational.builder.part.structure.Structure;

public class DetachedHouseBuilder extends HouseBuilder {
    @Override
    public Basement getBasement() {
        return new ConcreteBasement();
    }

    @Override
    public Structure getStructure() {
        return new MetalStructure();
    }

    @Override
    public Roof getRoof() {
        return new MetalRoof();
    }

    @Override
    public Interior getInterior() {
        return new ModernStyleInterior();
    }
}
