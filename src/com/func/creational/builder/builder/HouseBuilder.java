package com.func.creational.builder.builder;

import com.func.creational.builder.part.basement.Basement;
import com.func.creational.builder.part.interior.Interior;
import com.func.creational.builder.part.roof.Roof;
import com.func.creational.builder.part.structure.Structure;
import com.func.creational.builder.product.House;

public abstract class HouseBuilder {
    private House house;

    public HouseBuilder() {
        this.house = new House();
    }

    public House getResult() {
        return this.house;
    }

    public void buildBasement() {
        this.house.setBasement(this.getBasement());
    }

    public void buildStructure() {
        this.house.setStructure(this.getStructure());
    }

    public void buildRoof() {
        this.house.setRoof(this.getRoof());
    }

    public void buildInterior() {
        this.house.setInterior(this.getInterior());
    }

    protected abstract Basement getBasement();
    protected abstract Structure getStructure();
    protected abstract Roof getRoof();
    protected abstract Interior getInterior();
}
