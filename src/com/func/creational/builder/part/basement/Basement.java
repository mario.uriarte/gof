package com.func.creational.builder.part.basement;

public abstract class Basement {
    public abstract String getDescription();
}
