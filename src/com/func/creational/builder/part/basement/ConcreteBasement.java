package com.func.creational.builder.part.basement;

import com.func.creational.builder.part.basement.Basement;

public class ConcreteBasement extends Basement {
    @Override
    public String getDescription() {
        return "Concrete Basement";
    }
}
