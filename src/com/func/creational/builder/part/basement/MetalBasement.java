package com.func.creational.builder.part.basement;

import com.func.creational.builder.part.basement.Basement;

public class MetalBasement extends Basement {
    @Override
    public String getDescription() {
        return "Metal Basement";
    }
}
