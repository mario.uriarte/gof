package com.func.creational.builder.part.interior;

import com.func.creational.builder.part.basement.Basement;

public class ClassicStyleInterior extends Interior {
    @Override
    public String getDescription() {
        return "Classic Style";
    }
}
