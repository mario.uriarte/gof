package com.func.creational.builder.part.interior;

public abstract class Interior {
    public abstract String getDescription();
}
