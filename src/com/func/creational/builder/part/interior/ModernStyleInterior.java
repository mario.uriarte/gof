package com.func.creational.builder.part.interior;

public class ModernStyleInterior extends Interior {
    @Override
    public String getDescription() {
        return "Modern Style";
    }
}
