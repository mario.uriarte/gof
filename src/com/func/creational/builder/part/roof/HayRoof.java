package com.func.creational.builder.part.roof;

import com.func.creational.builder.part.basement.Basement;

public class HayRoof extends Roof {
    @Override
    public String getDescription() {
        return "Hay Roof";
    }
}
