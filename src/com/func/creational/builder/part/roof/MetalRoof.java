package com.func.creational.builder.part.roof;

public class MetalRoof extends Roof {
    @Override
    public String getDescription() {
        return "Metal Roof";
    }
}
