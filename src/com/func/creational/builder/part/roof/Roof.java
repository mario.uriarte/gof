package com.func.creational.builder.part.roof;

public abstract class Roof {
    public abstract String getDescription();
}
