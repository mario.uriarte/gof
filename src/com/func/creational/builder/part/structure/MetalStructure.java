package com.func.creational.builder.part.structure;

public class MetalStructure extends Structure {
    @Override
    public String getDescription() {
        return "Metal Structure";
    }
}
