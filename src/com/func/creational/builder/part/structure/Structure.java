package com.func.creational.builder.part.structure;

public abstract class Structure {
    public abstract String getDescription();
}
