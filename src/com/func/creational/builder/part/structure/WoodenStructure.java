package com.func.creational.builder.part.structure;

import com.func.creational.builder.part.roof.Roof;

public class WoodenStructure extends Structure {
    @Override
    public String getDescription() {
        return "Wooden Structure";
    }
}
