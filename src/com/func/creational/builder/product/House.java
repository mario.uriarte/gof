package com.func.creational.builder.product;

import com.func.creational.builder.part.basement.Basement;
import com.func.creational.builder.part.interior.Interior;
import com.func.creational.builder.part.roof.Roof;
import com.func.creational.builder.part.structure.Structure;

public class House {
    private Basement basement;
    private Structure structure;
    private Roof roof;
    private Interior interior;


    public Basement getBasement() {
        return basement;
    }

    public void setBasement(Basement basement) {
        this.basement = basement;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Roof getRoof() {
        return roof;
    }

    public void setRoof(Roof roof) {
        this.roof = roof;
    }

    public Interior getInterior() {
        return interior;
    }

    public void setInterior(Interior interior) {
        this.interior = interior;
    }
}
