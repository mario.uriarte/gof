package com.func.model;

import com.func.behavioral.memento.MementoCar;
import com.func.structural.decorator.Vehicle;

public class Car implements Vehicle {
    private String model;
    private String price;
    private String color;
    private String brand;

    public Car(String model, String price, String color, String brand) {
        this.model = model;
        this.price = price;
        this.color = color;
        this.brand = brand;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void showCar() {
        System.out.println(this.brand + " - " + this.price + " - " + this.color + " - " + this.model);
    }

    public MementoCar saveMemento() {
        System.out.println("Save state memento...");
        MementoCar mementoCar = new MementoCar(this.model, this.price, this.color, this.brand);
        return mementoCar;
    }

    public Car restoreMemento(MementoCar mementoCar) {
        System.out.println("Restore state memento...");
        Car stateCar = new Car(
                mementoCar.getModel(),
                mementoCar.getPrice(),
                mementoCar.getColor(),
                mementoCar.getBrand()
        );
        return stateCar;
    }

    @Override
    public void gas() {
        System.out.println("Gas go og go...");
    }

    @Override
    public void stop() {
        System.out.println("Stop car...");

    }

    @Override
    public void start() {
        System.out.println("Start car...");

    }
}
