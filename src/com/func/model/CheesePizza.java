package com.func.model;

public class CheesePizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("Preparing whit extra cheese");
    }

    @Override
    public String getName() {
        return "Cheese Pizza";
    }
}
