package com.func.model;

public class PepperoniPizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("Preparing whit peperoni");
    }

    @Override
    public String getName() {
        return "Peperoni Pizza";
    }
}
