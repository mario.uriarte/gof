package com.func.model;

public abstract class PizzaStore {
    public abstract Pizza createPizza(PizzaType pizzaType);

    public Pizza order(PizzaType pizzaType) {

        Pizza pizza = this.createPizza(pizzaType);

        if (pizza == null) {
            System.out.println("We don't have that pizza!");
        } else {
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }

        return pizza;
    }
}
