package com.func.model;

public class SczPizzaStore extends PizzaStore {

    @Override
    public Pizza createPizza(PizzaType pizzaType) {
        Pizza pizza;

        if (pizzaType.equals(PizzaType.CHEESE)) {
            pizza = new CheesePizza();
        } else if (pizzaType.equals(PizzaType.PEPERONI)) {
            pizza = new PepperoniPizza();
        } else if (pizzaType.equals(PizzaType.VEGGIE)) {
            pizza = new VeggiePizza();
        } else {
            pizza = null;
        }

        return pizza;
    }
}
