package com.func.model;

public class VeggiePizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("Preparing veggie pizza");
    }

    @Override
    public String getName() {
        return "Veggie Pizza";
    }
}
