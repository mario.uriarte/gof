package com.func.structural.composite;

import com.func.structural.composite.component.FileComponent;
import com.func.structural.composite.composite.Directory;
import com.func.structural.composite.leaf.File;

/**
 * Composite
 * We need to support a directory tree and also we want to  display it
 */

public class Client {

    public static void test() {

        Directory root = new Directory("Root");
        Directory folder1 = new Directory("Folder1");
        Directory folder2 = new Directory("Folder2");

        FileComponent file1 = new File("File1", "gif");
        FileComponent file2 = new File("File2", "gif");

        Directory subFolder1 = new Directory("SubFolder1");
        FileComponent subFile1 = new File("subFile1", "txt");

        Directory subFolder2 = new Directory("SubFolder2");
        Directory subFolder3 = new Directory("SubFolder3");
        FileComponent file1b = new File("File1", "jpg");
        FileComponent file2b = new File("File1", "png");

        subFolder1.addFileComponent(subFile1);
        folder1.addFileComponent(subFolder1);
        folder1.addFileComponent(subFolder2);
        folder1.addFileComponent(subFolder3);
        folder1.addFileComponent(file1b);
        folder1.addFileComponent(file2b);

        root.addFileComponent(folder1);
        root.addFileComponent(folder2);
        root.addFileComponent(file1);
        root.addFileComponent(file2);

        root.display(0);

//        File song = new File("Los pollitos", "mp3");
//        song.display();

        // 29:20
        System.out.println("\n**************\n");

        subFolder1.display(0);

        System.out.println("\n**************\n");

    }
}
