package com.func.structural.composite.component;

public abstract class FileComponent {
    protected String name;

    public FileComponent(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    protected String getIndentation(int indent) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < indent; i++) {
            sb.append("    ");
        }
        return sb.toString();
    }

    public abstract void display(int indent);
}
