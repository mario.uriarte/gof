package com.func.structural.composite.composite;

import com.func.structural.composite.component.FileComponent;

import java.util.ArrayList;
import java.util.List;

public class Directory extends FileComponent {
    private List<FileComponent> subFileComponents;


    public Directory(String name) {
        super(name);

        this.subFileComponents = new ArrayList<FileComponent>();
    }

    public void addFileComponent(FileComponent fileComponent) {
        this.subFileComponents.add(fileComponent);
    }

    @Override
    public void display(int indent) {
        System.out.println(this.getIndentation(indent) + this.name);

        for (FileComponent fc: this.subFileComponents) {
            fc.display(indent + 1);
        }

    }
}
