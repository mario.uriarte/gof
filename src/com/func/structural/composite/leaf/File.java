package com.func.structural.composite.leaf;

import com.func.structural.composite.component.FileComponent;

public class File extends FileComponent {
    private String extension;

    public File(String name, String extension) {
        super(name);
        this.extension = extension;
    }

    @Override
    public void display(int indent) {
        System.out.println(
                this.getIndentation(indent) +
                this.name + "." + this.extension);
    }
}
