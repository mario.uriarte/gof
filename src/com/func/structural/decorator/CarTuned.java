package com.func.structural.decorator;

public class CarTuned extends VehicleDecorator {

    public CarTuned(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public void gas() {
        getVehicle().gas();
        System.out.println("Now whit nitrous oxide...");
    }

    @Override
    public void stop() {
        getVehicle().stop();
        System.out.println("Now with abs brakes...");
    }

    @Override
    public void start() {
        getVehicle().start();
        System.out.println("Now checking all systems...");

    }
}
