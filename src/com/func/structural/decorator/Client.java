package com.func.structural.decorator;

import com.func.model.Car;

public class Client {
    public static void test() {
        System.out.println("Hi from decorator app");

        Car car1 = new Car(
                "2018",
                "19000$",
                "Red",
                "Mazda"
        );
        car1.showCar();
        car1.start();
        car1.gas();
        car1.stop();

        System.out.println("<><><><><><><>");

        Vehicle car2 = new Car(
                "1995",
                "39000$",
                "Blue",
                "Camaro"
        );

        car2 = new CarTuned(car2);
        car2.start();
        car2.gas();
        car2.stop();
    }
}
