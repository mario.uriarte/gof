package com.func.structural.decorator;

public interface Vehicle {
    public void gas();
    public void stop();
    public void start();
}
