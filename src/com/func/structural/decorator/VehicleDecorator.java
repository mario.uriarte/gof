package com.func.structural.decorator;

public abstract class VehicleDecorator implements Vehicle {

    private Vehicle vehicle;

    public VehicleDecorator(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    protected Vehicle getVehicle() {
        return vehicle;
    }
}
